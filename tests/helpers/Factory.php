<?php

trait Factory {
	
	protected $times = 1;
	
	protected function getStub()
	{
		throw new \BadMethodCallException('Create your own getStub method to declare your fields');
	}

	protected function make($type, $fields = [])
	{			

		while($this->times--)
		{	
			$stubs = array_merge($this->getStub(), $fields);
			$type::create($stubs);
		}	
	}

	protected function times($count)
	{
		$this->times = $count;

		return $this;
	}

}