<?php


use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Lesson;

abstract class ApiTester extends TestCase {	
	

	protected $fake;
	

	public function __construct()
	{
		$this->fake = \Faker\Factory::create();
	}
	

	protected function getJson($uri, $method = 'GET', $parameters = [])
	{
		return json_decode($this->call($method, $uri, $parameters)->getContent());
	}

	protected function assertObjectHasAttributes()
    {
    	$args = func_get_args();

    	$object = array_shift($args);

    	foreach($args as $attribute)
    	{
    		$this->assertObjectHasAttribute($attribute, $object);
    	}	
    }
}