<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lesson;
use App\Transformers\LessonTransformer;
use App\Http\Requests;

class LessonsController extends ApiController
{	

	protected $lessonTransformer;

	public function __construct(LessonTransformer $lessonTransformer)
	{
		$this->lessonTransformer = $lessonTransformer;

		$this->middleware('auth.basic', ['only' => 'store']);
	}


	public function index(Request $request)
	{	
		$limit = $request->input('limit', 3);
		$lessons = Lesson::paginate($limit);
		

		return $this->respondWithPagination($lessons, [
				'data' => $this->lessonTransformer->transformCollection($lessons->all())
			]);	
		
	}


	public function show($id)
	{
		$lesson = Lesson::find($id);

		if(! $lesson)
		{
			return $this->respondNotFound('Lesson does not exists.');
		}

		return \Response::json([
			'data' => $this->lessonTransformer->transform($lesson)
			], 200);
	}
	
	public function store(Request $request)
	{
		if( ! $request->title || ! $request->body)
		{
			return $this->respondInvalidParameters();
		}

		Lesson::create([
			'title' => $request->title,
			'body' => $request->body,
			]);

		return $this->respondCreated('Lesson successfuly created.');
	}


}
