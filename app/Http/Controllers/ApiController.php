<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response as IlluminateResponse;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

use App\Http\Requests;

class ApiController extends Controller
{
    protected $statusCode = 200;

    public function getStatusCode()
    {
    	return $this->statusCode;
    }

    public function setStatusCode($statusCode)
    {
    	$this->statusCode = $statusCode;

    	return $this;
    }

    public function respondCreated($message)
    {
    	return $this->setStatusCode(IlluminateResponse::HTTP_CREATED)->respond([
				'message' => $message
			]);
    }

    public function respondInvalidParameters($message = 'Parameters not defined')
    {
    	return $this->setStatusCode(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY)->respondWithError($message);
    }

    public function respondNotFound($message = 'Not Found.')
    {
    	return $this->setStatusCode(IlluminateResponse::HTTP_NOT_FOUND)->respondWithError($message);	
    }

    public function respondInternalError($message = 'Internal Error')
    {
    	return $this->setStatusCode(IlluminateResponse::HTTP_INTERNAL_SERVER_ERROR)->respondWithError($message);
    }

    public function respond($data, $headers = [])
    {
    	return \Response::json($data, $this->getStatusCode(), $headers);
    }

    public function respondWithPagination(Paginator $lessons, $data)
    {   
        $data = array_merge($data, [
                 'paginator' => [
                    'total_count'  => $lessons->total(),
                    'total_pages'  => ceil($lessons->total() / $lessons->perPage()),
                    'current_page' => $lessons->currentPage(),
                    'limit'        => $lessons->perPage()
                ]
            ]);

        return $this->respond($data);

    }

    public function respondWithError($message)
    {
    	return $this->respond([
				'error' => [
					'message'     => $message,
					'status_code' => $this->getStatusCode()
					]
				]);
    }
}
