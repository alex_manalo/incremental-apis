<?php

use App\Lesson;
use App\Tag;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{   
    protected $tables = [
            'lessons',
            'tags',
            'lesson_tag'
        ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {	
        $this->cleanDatabase();


        $this->call(LessonsTableSeeder::class);
        $this->call(TagTableSeeder::class);
        $this->call(LessonTagTableSeeder::class);
    }

    private function cleanDatabase()
    {
        //1701 Cannot truncate a table referenced in a foreign key constraint
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        
        foreach($this->tables as $table)
        {
            \DB::table($table)->truncate();
        }

        DB::statement('SET FOREIGN_KEY_CHECKS=1');   
    }
}
