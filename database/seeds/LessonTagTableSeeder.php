<?php

use Illuminate\Database\Seeder;

use App\Lesson;
use App\Tag;

class LessonTagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        $lessonIds = Lesson::lists('id')->all();
        $tagIds = Tag::lists('id')->all();

        foreach(range(1, 30) as $index)
        {

        	\DB::table('lesson_tag')->insert([

        		'lesson_id' => $faker->randomElement($lessonIds),
        		'tag_id'    => $faker->randomElement($tagIds),

        		]);	

        }
    }
}
